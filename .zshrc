# paths and such
export ZSH=$HOME/.oh-my-zsh
export GOPATH="$HOME/go"
export PATH="$HOME/.cargo/bin:$HOME/.local/bin:$HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:`composer global config home 2>/dev/null`/vendor/bin:$HOME/.scripts:$GOPATH/bin:`ruby -e 'print Gem.user_dir'`/bin"
export GEM_HOME=$(ruby -e 'print Gem.user_dir')
[ -z "$DISPLAY" ] && [ -f "$HOME/.profile" ] && . "$HOME/.profile"

# zsh config
ZSH_THEME="theunraveler"
HIST_STAMPS="yyyy-mm-dd"
plugins=(git colemak z sudo zsh-syntax-highlighting zsh-autosuggestions)
source $ZSH/oh-my-zsh.sh
for f in $HOME/.shell.d/*.sh; do source $f; done

# general environment thangs
export LANG="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export EDITOR="$(which nvim)"

# conveniences
which keychain 2>&1 > /dev/null && eval $(keychain --eval --agents ssh -Q --quiet $(ls ~/.ssh/*.pub | sed 's/.pub$//g'))

# keybinding stuff
autoload -U select-word-style
select-word-style bash

if [[ $TERM = *xterm* ]] ; then
  bindkey "\e[3~" delete-char
  bindkey "\e[1;5D" backward-word
  bindkey "\e[1;5C" forward-word
  bindkey "\eOH" beginning-of-line
  bindkey "\eOF" end-of-line

  bindkey "\e[Z" reverse-menu-complete # Shift+Tab
fi

if [[ $TERM = *rxvt* ]] ; then
  bindkey "^w"    kill-region
  bindkey "\e[3~" delete-char
  bindkey "\eOd"  backward-word
  bindkey "\eOc"  forward-word
  bindkey "\e[5~" beginning-of-history
  bindkey "\e[6~" end-of-history
  bindkey "\e[7~" beginning-of-line
  bindkey "\e[8~" end-of-line

  bindkey -s "\e^B" "^A^Kbrowse\n"

  bindkey "\e[Z" reverse-menu-complete # Shift+Tab
fi

# Grab everything from the .shell.d directory
for f in ~/.shell.d/*.sh; do source $f; done;

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
