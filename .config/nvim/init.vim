if empty(glob("~/.local/share/nvim/site/autoload/plug.vim"))
  silent! execute '!curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dir https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * silent! PlugInstall
endif

set hidden
set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call plug#begin('~/.local/share/nvim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'airblade/vim-gitgutter'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/vimproc.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mattn/emmet-vim'
" Plug 'wincent/command-t'
" Plug 'StanAngeloff/php.vim', { 'for': 'php' }
" Plug 'lvht/phpcd.vim', { 'for': 'php', 'do': 'composer install' }
Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/echodoc.vim'
Plug 'joonty/vdebug'
Plug 'fatih/vim-go', { 'for': 'go' }
Plug 'evidens/vim-twig'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'w0rp/ale'
Plug 'ludovicchabant/vim-gutentags'
Plug 'majutsushi/tagbar'
Plug 'cakebaker/scss-syntax.vim', { 'for': 'scss' }
Plug 'posva/vim-vue', { 'for': 'vue' }
" Plug 'sbdchd/neoformat'
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
" Plug 'racer-rust/vim-racer', { 'for': 'rust' }
Plug 'ElmCast/elm-vim', { 'for': 'elm' }
Plug 'sjl/badwolf'
Plug 'pbrisbin/vim-syntax-shakespeare'
" Plug 'eagletmt/neco-ghc'
" Plug 'freitass/todo.txt-vim'

call plug#end()              " required
filetype plugin indent on    " required



""" COLORS
set t_Co=256
colorscheme slate
if &diff
    colorscheme murphy
endif



""" ALE
let g:ale_linters = {
\   'php': ['langserver', 'php', 'phpcs', 'phpmd'],
\   'rust': ['rls'],
\}
let g:ale_fixers = {
\   'javascript': ['eslint'],
\   'php': ['phpcbf'],
\   'rust': ['rustfmt'],
\}
let g:ale_fix_on_save = 1
let g:airline#extensions#ale#enabled = 1


""" DEOPLETE
let g:deoplete#enable_at_startup = 1
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
set noshowmode
let g:echodoc_enable_at_startup = 1
let g:deoplete#omni#input_patterns = {}
let g:deoplete#omni#input_patterns.css = '^\s\+\w\+\|\w\+[):;]\?\s\+\w*\|[@!]'
let g:deoplete#omni#input_patterns.javascript = '[^. *\t]\.\w*'
let g:deoplete#omni#input_patterns.md = '<[^>]*'
let g:deoplete#omni#input_patterns.sass = '^\s\+\w\+\|\w\+[):;]\?\s\+\w*\|[@!]'
let g:deoplete#omni#input_patterns.scss = '^\s\+\w\+\|\w\+[):;]\?\s\+\w*\|[@!]'
let g:deoplete#omni#input_patterns.typescript = '[^. *\t]\.\w*'
let g:deoplete#omni#input_patterns.xml = '<[^>]*'
let g:deoplete#omni#input_patterns.php = '\w+|[^. \t]->\w*|\w+::\w*'
let g:deoplete#omni_patterns = {}
let g:deoplete#omni_patterns.html = '<[^>]*'



""" AIRLINE
let g:airline_theme='term'
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
set statusline+=%#warningmsg#
" set statusline+=%{NeomakeStatuslineFlag()}
set statusline+=%{gutentags#statusline()}
set statusline+=%*



""" FZF
function! s:fzf_statusline()
  " Override statusline as you like
  highlight fzf1 ctermfg=161 ctermbg=251
  highlight fzf2 ctermfg=23 ctermbg=251
  highlight fzf3 ctermfg=237 ctermbg=251
  setlocal statusline=%#fzf1#\ >\ %#fzf2#fz%#fzf3#f
endfunction

autocmd! User FzfStatusLine call <SID>fzf_statusline()
nmap <C-F> :Files<CR>
nmap <C-P> :GFiles<CR>
nmap <C-T> :Tags<CR>
nmap <C-G> :Commits<CR>
nmap <C-L> :Lines<CR>

nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

let $FZF_DEFAULT_OPTS="--bind=ctrl-z:toggle"



""" GUTENTAGS
let g:gutentags_cache_dir = $HOME . '/.cache/vim-gutentags'



""" TAGBAR
let g:tagbar_type_css = {
      \ 'ctagstype' : 'Css',
      \ 'kinds'     : [
      \ 'c:classes',
      \ 's:selectors',
      \ 'i:identities'
      \ ]
      \ }
nmap <F8> :TagbarToggle<CR>


""" JIRACOMPLETE
let g:jiracomplete_url = 'https://bluetent.atlassian.net'
let g:jiracomplete_username = 'aaron'



""" NETRW Configuration
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25



""" LanguageClient
" Required for operations modifying multiple buffers like rename.

let g:LanguageClient_serverCommands = {
    \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
    \ 'javascript': ['javascript-typescript-stdio'],
    \ 'php': ['php', "$HOME/.config/composer/vendor/felixfbecker/language-server/bin/php-language-server.php"],
    \ }

" Automatically start language servers.
let g:LanguageClient_autoStart = 1

nnoremap <silent> K :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient_textDocument_rename()<CR>
nnoremap <silent> gs :call LanguageClient_workspace_symbol()<CR>
inoremap <silent> C-, :call LanguageClient_textDocument_completion()<CR>




""" VIM CORE SETTINGS
syntax enable
set mouse=a
set ignorecase
set smartcase
set incsearch
set showmatch
set relativenumber
set number
set si
set backspace=start,eol,indent
set laststatus=2
set noshowmode
set clipboard=unnamed
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set fillchars=vert:\ ,stl:\ ,stlnc:\ 




""" LANGUAGES

" rust
let g:rustfmt_autosave = 1
set hidden
let $RUST_SRC_PATH="/home/aaron/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"
let g:racer_cmd = "/home/aaron/.cargo/bin/racer" 
au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap <leader>gd <Plug>(rust-doc)


" php
function DoDrupal()
  let shellcmd = "php56 /usr/local/bin/drush core-status | grep -i 'drupal version'"
  let output = system(shellcmd)

  if v:shell_error
    return 0
  endif
  
  let g:ale_php_phpcbf_standard = 'Drupal'
  let g:ale_php_phpcs_standard = 'Drupal'
  map \c :!php56 /usr/local/bin/drush cc all<CR>
  map \r :!php56 /usr/local/bin/drush rr<CR>
  map \w :!tmux split-window -t .+ "php56 /usr/local/bin/drush watchdog-show --tail"<CR><CR>
  map \s :!tmux split-window -t .+ "php56 /usr/local/bin/drush sqlc"<CR><CR>

  return 1
endfunction

call DoDrupal()

let g:ale_php_langserver_use_global = 1

" haskell
let g:necoghc_enable_detailed_browse = 1
let g:haskellmode_completion_ghc = 0
autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc
au FileType haskell nnoremap <buffer> <F1> :HdevtoolsType<CR>
au FileType haskell nnoremap <buffer> <silent> <F2> :HdevtoolsClear<CR>

function! Hfmt() abort
  let path = expand('%:p')
  let format = 'hfmt -w '
  call system(format . path)
  edit!
endfunction

augroup fmt-haskell
  autocmd!
  autocmd BufWritePost *.hs call Hfmt()
augroup END

" elm
autocmd FileType elm setlocal tabstop=4 softtabstop=4 shiftwidth=4

" etc
" autocmd BufWritePre *.scss Neoformat
" autocmd BufWritePre *.css Neoformat


nmap \y :%w !xclip -sel clip<CR><CR>
vmap \y :'<,'>w !xclip -sel clip<CR><CR>


""" RELOCATE SWP/TMP FILES
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup
