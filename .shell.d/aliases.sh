(which nvim 2>&1 >/dev/null) && alias vim=nvim && alias vimdiff='nvim -d'
(which hub 2>&1 >/dev/null) && alias git=hub

if [ "$OSTYPE" = "linux-gnu" ]; then
  alias ls='ls --color=auto -v --group-directories-first' # nice colors and stuff
else
  alias ls='ls -G' # or...
fi

alias ls='ls -F' # classifiers
alias grep='grep --color=auto' # nice colors
alias mkdir='mkdir -p' # don't let me do anything stupid accidentally
alias rm='rm -i'    # REALLY don't let me do anything stupid accidentally
alias cls='printf "\ec"' # clear out the term
