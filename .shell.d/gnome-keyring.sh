if [ -n "$DESKTOP_SESSION" ] && which gnome-keyring-daemon 2>&1 >/dev/null; then
  eval $(gnome-keyring-daemon --start)
  export SSH_AUTH_SOCK
fi
